package com.mygdx.game;

import java.util.ArrayList;

public class Dot {
    private int x;
    private int y;
    private ArrayList<Dot> neighbours;
    public float gScore;
    private float hScore;
    public float fScore;
    private int width=50;
    private int height=50;
    public Dot previous=null;
    public boolean wall;



    public Dot(int x, int y) {
        neighbours = new ArrayList<Dot>();
        this.x = x;
        this.y = y;
        if(Math.random()*7>5.7){
            wall=true;
        }
    }



    public ArrayList<Dot> getNeighbors(Dot[][] grid) {
        if (x < grid.length - 1) {
            neighbours.add(grid[x + 1][y]);
        }
        if (x > 0) {
            neighbours.add(grid[x - 1][y]);
        }
        if (y < grid.length - 1) {
            neighbours.add(grid[x][y + 1]);
        }
        if (y > 0) {
            neighbours.add(grid[x][y - 1]);
        }
        return neighbours;
    }

    public void setfScore(float fScore) {
        this.fScore = fScore;
    }


    public float gethScore() {
        return hScore;
    }

    public float getfScore() {
        return fScore;
    }
    public void sethScore(float hScore) {
        this.hScore = hScore;
    }
    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }

    public float getgScore() {
        return gScore;
    }

    public void setGScore(float score) {
        gScore = score;
    }

}
