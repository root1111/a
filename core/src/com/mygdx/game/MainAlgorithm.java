package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;


import java.util.ArrayList;

public class MainAlgorithm extends ApplicationAdapter {

    private A_Start a_start;
    private Dot[][] dots;
    private ShapeRenderer renderer;
    private float timer;
    private Thread drawThread;



    @Override
    public void create() {
        a_start = new A_Start();
        dots = a_start.getGrid();
        renderer = new ShapeRenderer();
        renderer.setAutoShapeType(true);
        a_start.doAStar();

    }


	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		renderer.begin();

		for(int i=0;i<dots.length;i++){
			for(int j=0;j<dots.length;j++){
				Dot dot=dots[i][j];
				if(dot.wall){
					renderer.set(ShapeRenderer.ShapeType.Filled);
					renderer.setColor(Color.BLUE);
				}else{
					renderer.set(ShapeRenderer.ShapeType.Point);
					renderer.setColor(Color.WHITE);
				}
				renderer.rect(dot.getX()*55,dot.getY()*55,15,15);
			}
		}

		renderer.set(ShapeRenderer.ShapeType.Filled);
		renderer.setColor(Color.WHITE);
        for (Dot dot : a_start.getPath()) {
            renderer.rect(dot.getX() * 55, dot.getY() * 55, 15, 15);
        }
        renderer.end();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose () {

	}

}
