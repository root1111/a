package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.ArrayList;

public class A_Start {
    private static int rowCount = 18;
    private static int columnCount = 30;
    private Table table;
    private Stage stage;
    private Dot[][] grid = new Dot[rowCount][columnCount];
    private ArrayList<Dot> closedSet;
    private ArrayList<Dot> openSet;
    private Dot startDot;
    private Dot end;
    private float testScore;
    private  ArrayList<Dot> path;

    public A_Start() {
        path=new ArrayList<Dot>();
        openSet=new ArrayList<Dot>();
        closedSet=new ArrayList<Dot>();
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                Dot dot = new Dot(i, j);
                grid[i][j] = dot;
            }
        }

        startDot=grid[0][0];
        end=grid[rowCount-1][rowCount-1];
        grid[0][0].setGScore(0);
        openSet.add(startDot);
        end.wall=false;
        startDot.wall=false;

    }
    public void doAStar(){
        while (!openSet.isEmpty()) {
                Dot current = getLowestFscoreDot();
                if (current == end) {
                    Dot temp = current;
                    path.add(temp);
                    while (temp.previous != null) {
                        path.add(temp.previous);
                        temp = temp.previous;
                    }
                    return;
                }
                openSet.remove(current);
                closedSet.add(current);
                for (Dot neighbor : current.getNeighbors(grid)) {

                    if (!closedSet.contains(neighbor) && !neighbor.wall) {
                        testScore = current.getgScore() + dist(current, neighbor);
                        if (!openSet.contains(neighbor)) {
                            openSet.add(neighbor);
                        } else if (testScore >= neighbor.getgScore()) {
                            continue;
                        }
                        neighbor.previous = current;
                        neighbor.gScore = (testScore);
                        neighbor.fScore = (neighbor.getgScore() + dist(neighbor, end));

                    }

                }
            }
        }

    private float dist(Dot one,Dot two){
        return (float) Math.sqrt(Math.pow(two.getX()-one.getX(),2)+Math.pow(two.getY()-one.getY(),2));
    }

    private Dot getLowestFscoreDot(){
        float lowesScore=0;
        Dot lowestScoreDot=openSet.get(0);
        for(Dot dot:openSet){
            if(dot.getfScore()<lowesScore){
                lowesScore=dot.getfScore();
                lowestScoreDot=dot;
            }
        }
        return lowestScoreDot;
    }

    public Dot[][] getGrid() {
        return grid;
    }

    public ArrayList<Dot> getPath() {
        return path;
    }
}
